﻿
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
	public Camera cam;
	public UnityEvent OnInteractionInput;
	private InputData input;
	private CharacterAnimBasedMovement characterMovement;

	public bool OnInteractionZone { get; set; }
	void Start() 
	{
		characterMovement = GetComponent<CharacterAnimBasedMovement>();
	}

	void Update()
	{
		input.getImput();

		if (OnInteractionZone && input.jump)
		{
			OnInteractionInput.Invoke();
		}

		characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);
	}
}
